import { useState } from 'react';
import ProductItem from "./ProductItem";

const ProductList = ({ items, setItems, activeItem, onItemClick }) => {
    const toggleStatus = (itemIndex) => {
        let tempItems = [...items];
        tempItems[itemIndex].status = tempItems[itemIndex].status === 'active' ? 'inactive' : 'active';
        setItems(tempItems);
    }
    return (
        <div>
            {items.map((item, index) =>
                <ProductItem
                    key={item.name}
                    item={item}
                    activeItem={activeItem}
                    onItemClick={onItemClick}
                    itemIndex={index}
                    setItems={setItems}
                    toggleStatus={toggleStatus}
                />)}
        </div>
    );
};

export default ProductList;