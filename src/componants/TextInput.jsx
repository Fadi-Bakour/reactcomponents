import { useState } from 'react'
import styles from './TextInput.module.css';
import viteLogo from '/vite.svg'

const TextInput = ({ maxItemCount, items, setItems }) => {
    const [inputValue, setInputValue] = useState('');
    const [textError, setTextError] = useState('');

    const handleChange = (e) => {
        if (maxItemCount <= items.length) {
            return;
        }
        const currentValue = e.target.value;
        const alphanumericRegex = /^[a-zA-Z0-9]*$/;

        if (currentValue === '' || alphanumericRegex.test(currentValue)) {
            setInputValue(currentValue);
            setTextError('');
        } else {
            setTextError('Please enter only alphanumeric characters.');
        }
    };

    const handleBlur = () => {
        if (maxItemCount >= items.length) {
            setTextError('')
        }
        if (inputValue === '') {
            setTextError('This field cannot be empty.');
        }
    };

    const handleFocuse = () => {
        setTextError('')
    };

    const addItem = () => {
        if (maxItemCount <= items.length || inputValue.trim() === '') {
            return;
        }
        let tempItems = [...items];
        tempItems.push({
            name: inputValue,
            quantity: '0',
            image: viteLogo,
            status: 'inactive'
        });
        setItems(tempItems);
        setInputValue('')
    }

    return (
        <div>
            <div className={styles.main}>
                <input
                    type="text"
                    value={inputValue}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    onFocus={handleFocuse}
                    autoFocus
                    style={{ border: textError ? '1px solid red' : '1px solid black' }}
                    disabled={maxItemCount <= items.length ? true : false}
                />
                <div onClick={addItem} className={styles.add_btn}>
                    ADD
                </div>
            </div>
            {textError && <p style={{ color: 'red' }}>{textError}</p>}
        </div>
    )
};

export default TextInput;