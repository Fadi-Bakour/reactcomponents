import styles from "./ProductItem.module.css"

const ProductItem = ({ item, activeItem, onItemClick, itemIndex, toggleStatus }) => {
    return (
        <div className={styles.main} onClick={() => { onItemClick(item.name) }}>
            <div className={styles.header}>
                <div  style={item.status === 'active' ? { color: 'green' } : { color: 'red' }}>{item.name}</div>
                <div className={styles.toggle_btn} onClick={() => { toggleStatus(itemIndex) }}>
                    {item.status === 'active' ? 'Deactivate' : 'Activate'}
                </div>
            </div>
            {item.name === activeItem ?
                <div className={styles.details}>
                    <img src={item.image} />
                    <p>
                        Quantity: {item.quantity}
                    </p>
                </div>
                : null}
        </div>
    );
};

export default ProductItem;