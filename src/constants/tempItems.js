import viteLogo from '/vite.svg'

const tempItems = [{
    name: 'item 1',
    quantity: '3',
    image: viteLogo,
    status: 'active'
}, {
    name: 'item 2',
    quantity: '1',
    image: viteLogo,
    status: 'inactive'
}, {
    name: 'item 3',
    quantity: '6',
    image: viteLogo,
    status: 'active'
}];

export default tempItems;