import { useState } from 'react';
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { TextInput, ProductList } from './componants';
import tempItems from './constants/tempItems';

const App = () => {
  const [items, setItems] = useState(tempItems);
  const [activeItem, setActiveItem] = useState();
  const [maxItemCount, setMaxItemCount] = useState(5);

  const onItemClick = (itemName) => {
    setActiveItem(itemName);
  }
  return (
    <>
      <div>
        <img src={reactLogo} className="logo react" alt="React logo" />
      </div>
      <div className="card">
        <TextInput maxItemCount={maxItemCount} items={items} setItems={setItems} />
      </div>
      <ProductList
        items={items}
        setItems={setItems}
        activeItem={activeItem}
        onItemClick={onItemClick}
        maxItemCount={maxItemCount}
      />
    </>
  )
}

export default App
